import requests
from os import system
import json 

TOKEN = ''
URL = 'http://127.0.0.1:5000/graphql'

def start_client():
    while True:
        print("1 - Авторизация")
        print("2 - Регистрация")

        num = input()
        if not num.isdigit() or int(num) > 2: 
            print ("Неправильная команда!")
            continue
        num = int(num)
        msg = {}
        if num == 1:
            authorization()
        if num == 2:
            add_client()
            continue
        

def authorization():
    system('cls')
    while True:
        print("АВТОРИЗАЦИЯ")
        print("Логин:")
        username = input()
        print("Пароль:")
        pwd = input()
        headers = {'Content-type': 'application/json'}
        query = {
            'query': 'mutation{auth(login:"'+ username +'",password:"'+ pwd +'"){ token }}'
        }
        response = requests.post(URL, data=json.dumps(query), headers=headers)
        if response.status_code == 200:
            answer = json.loads(response.text)
            if answer['data']['auth'] == None:
                print(answer['errors'][0]['message'])
                start_client()
            else:
                print("УСПЕХ")
                global TOKEN
                TOKEN = answer['data']['auth']['token']
                menu()
                break
        else:
            print("Ошибка: " + str(response.status_code))
            continue

def add_client():
    client = {}
    print("Логин:")
    login = input()
    while True:
        print("Пароль:")
        pass1 = input()
        print("Повторите пароль:")
        pass2 = input()

        if pass1 != pass2:
            print("Пароли не совпадают!")
            continue
        else:
            headers = {'Content-type': 'application/json'}
            query = {
                'query': 'mutation{add_client(login:"'+ login +'",password:"'+ pass1 +'"){ client_id }}'
            }
            response = requests.post(URL, data=json.dumps(query), headers=headers)
            system('cls')
            if response.status_code == 200:
                answer = json.loads(response.text)
                if answer['data']['add_client'] == None:
                    print(answer['errors'][0]['message'])
                    start_client()
                else:
                    print("УСПЕХ")
                    break
            else:
                print("Ошибка: " + str(response.status_code))
                start_client()
            
def menu():
    while True:
        print("Главное меню")
        print("1 - Мои приемы")
        print("2 - Записаться на прием")
        print("3 - Отменить прием")
        print("4 - Мои питомцы")
        print("5 - Выйти")

        num = input()
        if not num.isdigit() or int(num) > 10: 
            print ("Неправильная команда!")
            continue
        num = int(num)
        msg = {}
        if num == 1:
            system('cls')
            get_visits()
        if num == 2:
            system('cls')
            add_visit()
        if num == 3:
            system('cls')
            cancel_visit()
        if num == 4:
            menu_pet()
            system('cls')
        if num == 5:
            exit(0) 
 

def cancel_visit():
    visits = get_visits()
    if(visits != -1 and visits != 0):
        while True:
            print("0 - назад")
            id = input("Введите id приема: ")
            if not id.isdigit(): 
                print ("Неправильная команда!")
                continue
            id = int(id)
            if id == "0":
                menu()
            if id > 0:
                visit = id
                k = 0
                v = ''
                for visit in visits:
                    if visit['id'] == id:
                        k = 1
                        v = visit
                        break
                if k > 0:
                    if v["status"] == "актуальный":
                        headers = {'Content-type': 'application/json', 'token':TOKEN}
                        query = {
                            'query': 'mutation{cancel_visit(visit_id:'+str(id)+')}'
                        }
                        response = requests.post(URL, headers=headers, data=json.dumps(query))
                        if response.status_code == 200:
                            answer = json.loads(response.text)
                            if answer['data']['cancel_visit'] == None:
                                print(answer['errors'][0]['message'])
                                menu()
                            elif answer['data']['cancel_visit'] == True:
                                print('УСПЕХ')
                                menu()
                            else:
                                print('ошибка')
                                menu()

                        else:
                            print("Ошибка: " + str(response.status_code))
                else:
                    print("Нет приема с таким номером")
                    continue
    else:
        print ("Нет приемов для отмены")
        menu()

def menu_pet():
    while True:
        print("Меню: ПИТОМЦЫ")
        print("1 - Показать моих питомцев")
        print("2 - Добавить питомца")
        print("3 - Показать приемы питомца")
        print("4 - Главное меню")
        num = input()
        if not num.isdigit() or int(num) > 6: 
            print ("Неправильная команда!")
            continue
        num = int(num)
        msg = {}
        if num == 1:
            system('cls')
            get_pets_by_client()
        elif num == 2:
            system('cls')
            add_pet()
        elif num == 3:
            system('cls')
            get_visit_by_pet()
        elif num == 4:
            menu()
        else:
            print ("Неправильная команда!")
            continue
        menu()

def get_pets_by_client():
    headers = {'Content-type': 'application/json', 'token':TOKEN}
    query = {
        'query': '{get_pets_by_client{ id name kind {value} sex}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_pets_by_client'] == None:
            print(answer['errors'][0]['message'])
            return -1
        else:
            pets = answer['data']['get_pets_by_client']
            print_pets(pets)
            return pets
    else:
        print("Ошибка: " + str(response.status_code))
        menu()


def print_pets(pets):
    for pet in pets:
        print ("%s - %s - %s - %s" % (
            pet["id"], 
            pet["name"],
            pet["sex"],
            pet["kind"]['value']))
    print()

def get_visits():
    headers = {'Content-type': 'application/json', 'token':TOKEN}
    query = {
        'query': '{get_visit_by_client{ id datetime filial {address} status cabinet duration doctor{surname name} pet{name}}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_visit_by_client'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_visit_by_client'] == []:
            print('Не найдено')
            menu()
        else:
            visits = answer['data']['get_visit_by_client']
            print_visits(visits)
            return visits
    else:
        print("Ошибка сервера: " + str(response.status_code))

def print_visits(visits):
    system('cls')
    for visit in visits:
        print ("%s - %s - %s - %s - %s - %s - %s - %s" % (
            visit["id"], 
            visit["datetime"],
            visit["status"],
            visit["cabinet"],
            visit["doctor"]["surname"] + ' ' + visit["doctor"]["name"],
            visit["filial"]['address'],
            visit["pet"]["name"],
            visit["duration"]))
    print()

def select_kind():
    headers = {'Content-type': 'application/json'}
    query = {
        'query': '{get_all_kinds{ id value}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_all_kinds'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_all_kinds'] == []:
            print('Не найдено')
            menu()
        else:
            kinds = answer['data']['get_all_kinds']
            sel_kind = print_kinds(kinds)
            return sel_kind
    else:
        print("Ошибка: " + str(response.status_code))

def print_kinds(kinds):
    while True:
        print()
        print("Выберите вид животного из списка:")
        for kind in kinds:
            print ("%s - %s" % (
                kind["id"], 
                kind["value"]))
        kind = input()
        try:
            k = int(kind)
            if kinds[k]:
                return kind
            else:
                continue
        except:
            continue

def add_pet():
    print("Создание питомца")
    print("Кличка:")
    pet_name = input()
    print()
    pet_sex = 0
    while True:
        print("Пол (0 - ж, 1 - м):")
        s = input()
        if s == '0':
            pet_sex = False
            break
        elif s == '1':
            pet_sex = True
            break
        else:
            continue
    
    pet_kind_id = select_kind()
    
    headers = {'Content-type': 'application/json', 'token':TOKEN}
    query = {
        'query': 'mutation{add_pet(input:{pet_name:"'+pet_name+'" pet_kind_id:'+pet_kind_id+' pet_sex:'+str(pet_sex).lower()+'})}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['add_pet'] == None:
            print(answer['errors'][0]['message'])
            menu()
        else:
            print('УСПЕХ!')
        menu()
    else:
        print("Ошибка сервера: " + str(response.status_code))

    # return pet

def get_visit_by_pet():
    pets = get_pets_by_client()
    if len(pets) > 0:
        while True:
            print("0 - назад")
            pet_id = input("Введите id питомца: ")
            if not pet_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if pet_id == "0":
                menu()
            headers = {'Content-type': 'application/json', 'token':TOKEN}
            query = {
                'query': '{get_visit_by_pet(pet_id:'+pet_id+'){ id datetime filial {address} status cabinet duration doctor{surname name} pet{name}}}'
            }
            response = requests.post(URL, headers=headers, data=json.dumps(query))
            if response.status_code == 200:
                answer = json.loads(response.text)
                if answer['data']['get_visit_by_pet'] == None:
                    print(answer['errors'][0]['message'])
                elif answer['data']['get_visit_by_pet'] == []:
                    print('Не найдено')
                    return 0
                else:
                    visits = answer['data']['get_visit_by_pet']
                    print_visits(visits)
                    return visits
            else:
                print("Ошибка: " + str(response.status_code))
    else:
        print ("У вас нет зарегистрированных питомцев")

def get_all_services():
    headers = {'Content-type': 'application/json'}
    query = {
        'query': '{get_all_services{ id name cost}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_all_services'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_all_services'] == []:
            print("Услуги не найдены")
            return 
        else:
            services = answer['data']['get_all_services']
            print_services(services)
            return services
    else:
        print("Ошибка: " + str(response.status_code))
        menu()

def print_services(services):
    for service in services:
        print ("%s - %s - %s" % (
            service["id"], 
            service["name"],
            service["cost"]))
    print()

def get_all_filials():
    headers = {'Content-type': 'application/json'}
    query = {
        'query': '{get_all_filials{ id address}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_all_filials'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_all_filials'] == []:
            print("Филиалы не найдены")
            return 0
        else:
            filials = answer['data']['get_all_filials']
            print_filials(filials)
            return filials
    else:
        print("Ошибка: " + str(response.status_code))

def print_filials(filials):
    for filial in filials:
        print ("%s - %s" % (
            filial["id"], 
            filial["address"]))
    print()

def get_workers_by_service(service_id):
    headers = {'Content-type': 'application/json'}
    query = {
        'query': '{get_workers_by_service(service_id:'+service_id+'){ id surname name}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_workers_by_service'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_workers_by_service'] == []:
            print("Врачи не найдены")
            return 0
        else:
            workers = answer['data']['get_workers_by_service']
            print_workers(workers)
            return workers
    else:
        print("Ошибка: " + str(response.status_code))
        menu()

def print_workers(workers):
    for worker in workers:
        print ("%s - %s - %s" % (
            worker["id"], 
            worker["surname"],
            worker["name"]))
    print()

def get_free_date_schedule(filial_id, worker_id): 
    headers = {'Content-type': 'application/json'}
    query = {
        'query': '{get_free_date_schedule(filial_id:'+filial_id+' worker_id:'+worker_id+'){ schedule_id date}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_free_date_schedule'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_free_date_schedule'] == []:
            print("Даты не найдены")
            return 0
        else:
            dates = answer['data']['get_free_date_schedule']
            print_dates(dates)
            return dates
    else:
        print("Ошибка: " + str(response.status_code))

def print_dates(dates):
    for date in dates:
        print("%s - %s" % (date['schedule_id'], date['date']))
        print()


def get_free_time_schedule(schedule_id, service_id):
    headers = {'Content-type': 'application/json'}
    query = {
        'query': '{get_free_time_schedule(schedule_id:'+schedule_id+' service_id:'+service_id+'){ id time}}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['get_free_time_schedule'] == None:
            print(answer['errors'][0]['message'])
            menu()
        elif answer['data']['get_free_time_schedule'] == []:
            print("Свободное время не найдено")
            return 0
        else:
            times = answer['data']['get_free_time_schedule']
            print_times(times)
            return times
    else:
        print("Ошибка: " + str(response.status_code))

def print_times(times):
    for time in times:
        print("%s - %s" % (time['id'], time['time']))
    print()


def add_visit():
    system('cls')
    print("Запись на прием")
    pet_id = ''
    service_id = ''
    filial_id = ''
    doctor_id = ''
    date = ''
    schedule_id = ''
#питомец
    print("1. Выбор питомца")
    pets = get_pets_by_client()
    if len(pets) > 0:
        while True:
            print("0 - назад")
            pet_id = input("Введите id питомца: ")
            if not pet_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if pet_id == "0":
                menu()
            k = 0
            for pet in pets:
                if int(pet_id) == pet['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер питомца из списка!")
                continue
            break
    else:
        system('cls')
        print ("У вас нет зарегистрированных питомцев")
        menu()
#услуга
    system('cls')
    print("2. Выбор услуги")
    services = get_all_services()
    if services != 0 and services != -1:
        print()
        while True:
            print("0 - меню")
            service_id = input("Введите id услуги: ")
            if not service_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if service_id == "0":
                menu()
            k = 0
            for service in services:
                if int(service_id) == service['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер услуги из списка!")
                continue
            break
    else:
        system('cls')
        print ("Нет услуг для записи")
        menu()
    system('cls')
#филиал
    system('cls')
    print("3. Выбор филиала")
    filials = get_all_filials()
    if filials != 0 and filials != -1:
        print()
        while True:
            print("0 - меню")
            filial_id = input("Введите id филилала: ")
            if not filial_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if filial_id == "0":
                menu()

            k = 0
            for filial in filials:
                if int(filial_id) == filial['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер филила из списка!")
                continue
            break
    else:
        print ("Нет филиала для выбора")
        menu()
#врач
    system('cls')
    print("4. Выбор врача")
    workers = get_workers_by_service(service_id)
    if workers != 0 and workers != -1:
        print()
        while True:
            print("0 - меню")
            worker_id = input("Введите id врача: ")
            if not worker_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if worker_id == "0":
                menu()

            k = 0
            for worker in workers:
                if int(worker_id) == worker['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер врача из списка!")
                continue
            break
    else:
        print ("Нет врача для выбора")
        menu()
#даты
    system('cls')
    print("5. Выбор даты")
    dates = get_free_date_schedule(filial_id, worker_id)
    if dates != 0 and dates != -1:
        while True:
            print("0 - меню")
            schedule_id = input("Введите id даты: ")
            if not schedule_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if schedule_id == "0":
                menu()
            k = 0
            for date in dates:
                if schedule_id == str(date['schedule_id']):
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер даты из списка!")
                continue
            system('cls')
            break
    else:
        print ("Нет дат для выбора")
        menu()
#время
    time = ''
    system('cls')
    print("6. Выбор времени")
    times = get_free_time_schedule(schedule_id, service_id)
    if times != 0 and times != -1 and times != None:
        print()
        while True:
            print("0 - меню")
            time_id = input("Введите id времени: ")
            if not time_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if time_id == "0":
                menu()
            k = 0
            for time in times:
                if time_id == str(time['id']):
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер времени из списка!")
                continue
            time = times[int(time_id)]['time']
            break
    else:
        print ("Нет времени для выбора")
        menu()

    headers = {'Content-type': 'application/json', 'token':TOKEN}
    query = {
        'query': 'mutation{add_visit(input:{pet_id:'+pet_id+' date:"'+time+'" worker_id:'+worker_id+' filial_id: '+filial_id+' service_id:'+service_id+'})}'
    }
    response = requests.post(URL, headers=headers, data=json.dumps(query))
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['data']['add_visit'] == None:
            print(answer['errors'][0]['message'])
            menu()
        else:
            print('УСПЕХ!')
        menu()

    else:
        print("Ошибка: " + str(response.status_code))









start_client()
