import json 
from flask import Flask, request, flash, redirect, json

import hashlib
import uuid

from sqlalchemy.dialects.mysql import insert
import sqlalchemy
from sqlalchemy import orm, and_, text, func
import redis
from models import *
import random
import string
import datetime

import win32api
import win32event

from ariadne import QueryType, graphql_sync, make_executable_schema, MutationType
from ariadne.constants import PLAYGROUND_HTML
from graphql import GraphQLError

from flask import Flask, request, jsonify


POOL = redis.ConnectionPool(host='127.0.0.1', port=6379, db=3)
engine = sqlalchemy.create_engine('mysql+pymysql://maria:1234@localhost/vc_2', pool_size=10, max_overflow=-1)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'vet_clinic'

salt = 'dfkgdfkgj'

#GRAPHQL##############################################################

type_defs = """
    type Service {
        id: Int
        name: String
        description: String
        duration: Int
        cost: Float
    }
    type Kind {
        id: Int
        value: String
    }
    type Filial {
        id: Int
        address_full: String
        address: String
        mail: String
    }
    type Pet {
        id: Int
        name: String
        sex: String
        date_of_birth: String
        kind: Kind
        client: Client
    }
    type Client {
        id: String,
        surname: String,
        name: String,
        patronymic: String,
        phone: String,
        mail: String,
        photo: String,
        date_of_birth: String,
        user_id: User
    }
    type User {
        id: Int
    }
    type Worker {
        id: Int
        surname: String
        name: String
        patronymic: String
        phone: String
        mail: String
        date_of_birth: String
        info: String
        position_id: Int
    }
    type Visit {
        id: Int
        datetime: String
        status: String
        cabinet: String
        duration: Int
        filial: Filial
        pet: Pet
        doctor: Worker
    }
    type FreeDates {
        schedule_id: Int
        date: String
    }
    type FreeTimes {
        id: Int
        time: String
    }
    type Query {
        hello: String!
        get_all_services: [Service]!
        get_all_kinds: [Kind]!
        get_all_filials: [Filial]!
        get_pets_by_client: [Pet]!
        get_workers_by_service(service_id: Int): [Worker]!
        get_visit_by_client: [Visit]!
        get_visit_by_pet(pet_id: Int): [Visit]!
        get_free_date_schedule(filial_id: Int, worker_id: Int): [FreeDates]!
        get_free_time_schedule(schedule_id: Int, service_id: Int): [FreeTimes]!
    }

    type Token {
        token: String
    }

    type NewUser {
        user_id: Int
        client_id: Int
    }

    input newVisit {
        date: String!
        filial_id: Int!
        pet_id: Int!
        worker_id: Int!
        service_id: Int!
    }

    input newPet {
        pet_name: String
        pet_kind_id: Int
        pet_sex: Boolean
    }

    type Mutation {
        auth(login: String, password: String): Token
        add_client(login: String, password: String): NewUser
        add_visit(input: newVisit): Int
        add_pet(input: newPet): Int
        cancel_visit(visit_id: Int): Boolean
    }
"""

query = QueryType()
mutation = MutationType()

#####################################################################

@mutation.field("auth")
def resolve_auth(_, info, login, password):
    logging('auth')
    pwd = password
    Session = sessionmaker(bind=engine)
    session = Session()
    user_q = session.query(User).filter_by(username = login).all()
    user = {}
    for row in user_q:
        user = dict(id = row.id, password = row.password)
        password = user['password']
        if password !=  hashlib.sha256(salt.encode() + pwd.encode()).hexdigest():
            raise Exception('Неверный пароль')

    if not user: 
        raise Exception('Неверный логин')
    token = ""
    
    token = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(30))

    update_stmt = user_table.update().values(token = token).where(user_table.c.id == user["id"])
    conn = engine.connect()
    result = conn.execute(update_stmt)

    return {"token":token}

@mutation.field("add_client")
def add_client(_, info, login, password):
    logging('add_client')
    pwd = hashlib.sha256(salt.encode() + password.encode()).hexdigest()

    try:
        insert_stmt = insert(User).values(username=login, password=pwd)
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        user_id = result.inserted_primary_key
        try:
            insert_stmt = insert(Client).values(
            user_id = user_id)
            result = conn.execute(insert_stmt)
            client_id = result.inserted_primary_key
            

            answer = dict(user_id=user_id[0], client_id=client_id[0])
            return answer
        except:
            raise Exception("Клиент не создан")
    except:
        raise Exception("пользователь не создан")


#SERVICE#############################################################

@query.field("get_all_services")
def resolve_get_all_services(_, info):
    logging('get_all_services')
    cache = get_cache("get_all_services", 0)
    try:
        rows = cache["get_all_services"]
        return rows
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        services_q = session.query(Service).filter_by(visibility = True, date_of_delete = None).all()
        services = []
        for row in services_q:
            service = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                duration = row.duration,
                cost = row.cost,
                nurse = row.nurse)
            services.append(service)
        if services:
            set_cache("get_all_services", services, cache["set"])
        return services

def get_service(id):
    Session = sessionmaker(bind=engine)
    session = Session()
    service_q = session.query(Service).filter_by(id = id).all()
    service = ''
    for row in service_q:
        service = dict(
            id = row.id,
            name = row.name,
            description = row.description,
            duration = row.duration,
            cost = row.cost,
            nurse = row.nurse)
    return service
 
#KIND################################################################

@query.field("get_all_kinds")
def resolve_get_all_kinds(_, info):
    logging('get_all_kinds')
    cache = get_cache("get_all_kinds", 0)
    try:
        rows = cache["get_all_kinds"]
        return rows
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        kinds_q = session.query(Kind).all()
        kinds = []
        for row in kinds_q:
            kind = dict(
                id = row.id,
                value = row.value)
            kinds.append(kind)
        if kinds:
            set_cache("get_all_kinds", kinds, cache["set"])
        return kinds

def get_kind(id):
    Session = sessionmaker(bind=engine)
    session = Session()
    kind_q = session.query(Kind).filter_by(id = id).all()
    kind = ''
    for row in kind_q:
        kind = dict(
            id = row.id,
            value = row.value)
    return kind

#Filial##############################################################
@query.field("get_all_filials")
def get_all_filials(_, info):
    logging('get_all_filials')
    cache = get_cache("get_all_filials", 0)
    try:
        rows = cache["get_all_filials"]
        return rows
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        filials_q = session.query(Filial).filter_by(visibility = True, date_of_delete = None).all()
        filials = []
        for row in filials_q:
            filial = dict(
                id = row.id,
                address_full = row.address_full,
                address = row.address,
                mail = row.mail)
            filials.append(filial)
        if filials:
            set_cache("get_all_filials", filials, cache["set"])
        return filials

def get_filial(id):
    Session = sessionmaker(bind=engine)
    session = Session()
    filials_q = session.query(Filial).filter_by(id = id).all()
    filial = ''
    for row in filials_q:
        filial = dict(
            id = row.id,
            address_full = row.address_full,
            address = row.address,
            mail = row.mail)
    return filial

#PET#################################################################
@query.field("get_pets_by_client")
def get_pets_by_client(_, info):
    logging('get_pets_by_client')
    token = request.headers['token']
    Session = sessionmaker(bind=engine)
    session = Session()
    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    pet_q = session.query(Pet).filter_by(client_id = client["id"]).all()
    pets = []
    for row in pet_q:
        if row.sex:
            sex = "мужской"
        else:
            sex = "женский"
        pet = dict(
            id = row.id,
            name = row.name,
            sex = sex,
            date_of_birth = row.date_of_birth,
            kind = get_kind(row.kind_id),
            client = client)
        pets.append(pet)
    return pets

def get_pet(token, id):
    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    Session = sessionmaker(bind=engine)
    session = Session()
    pet_q = session.query(Pet).filter_by(id = id).all()
    pet = ''
    for row in pet_q:
        pet = dict(
            id = row.id,
            name = row.name,
            sex = row.sex,
            date_of_birth = row.date_of_birth,
            kind = get_kind(row.kind_id),
            client = client)
    return pet

@mutation.field("add_pet")
def add_pet(_, info, input):
    logging('add_pet')
    token = request.headers['token']
    pet_name = input['pet_name']
    pet_kind_id = input['pet_kind_id']
    pet_sex = input['pet_sex']

    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    try:
        # if not "date_of_birth" in pet:
        #     date_of_birth = None
        # if not "name" in pet:
        #     name = None
        # if not "photo" in pet:
        #     photo = None
        # if not "sex" in pet:
        #     sex = 1
        # if not "kind_id" in pet:
        #     kind_id = None
        insert_stmt = insert(Pet).values(
            name = pet_name,
            sex = pet_sex,
            # photo = photo,
            # date_of_birth = date_of_birth,
            kind_id = pet_kind_id,
            client_id = client['id'])
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        pet_id = result.inserted_primary_key
        return pet_id[0]
    except:
        raise Exception('Ошибка, питомец не создан')

#CLIENT##############################################################

def get_client_by_user(token = ""):
    if token == "":
        token = request.headers['token']
    logging('get_client_by_user')
    Session = sessionmaker(bind=engine)
    session = Session()
    client_q = session.query(Client).join(User, User.id == Client.user_id).filter_by(token = token).all()
    client = {}
    for row in client_q:
        client = dict(
            id = row.id,
            surname = row.surname,
            name = row.name,
            patronymic = row.patronymic,
            phone = row.phone,
            mail = row.mail,
            photo = row.photo,
            date_of_birth = row.date_of_birth,
            user_id = row.user_id)
    return client

#WORKER##############################################################

@query.field("get_workers_by_service")
def get_workers_by_service(_, info, service_id):
    workers = get_workers_by_service_(service_id)
    return workers

def get_workers_by_service_(service_id):
    logging('get_workers_by_service')
    Session = sessionmaker(bind=engine)
    session = Session()
    worker_q = session.query(Worker).join(Worker_services, Worker_services.worker_id == Worker.id).filter_by(service_id = service_id).all()
    workers = []
    for row in worker_q:
        worker = dict(
            id = row.id,
            surname = row.surname,
            name = row.name,
            patronymic = row.patronymic,
            phone = row.phone,
            mail = row.mail,
            date_of_birth = row.date_of_birth,
            info = row.info,
            user_id = row.user_id,
            position_id = row.position_id)
        workers.append(worker)
    return workers

def get_worker(id):
    Session = sessionmaker(bind=engine)
    session = Session()
    worker_q = session.query(Worker).filter_by(id = id).all()
    worker = ''
    for row in worker_q:
        worker = dict(
            id = row.id,
            surname = row.surname,
            name = row.name,
            patronymic = row.patronymic,
            phone = row.phone,
            mail = row.mail,
            date_of_birth = row.date_of_birth,
            info = row.info,
            user_id = row.user_id,
            position_id = row.position_id)
    return worker

#VISIT###############################################################

@query.field("get_visit_by_client")
def get_visit_by_client(_, info):
    logging('get_visit_by_client')
    token = request.headers['token']
    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    Session = sessionmaker(bind=engine)
    session = Session()
    visit_q = session.query(Visit).filter_by(client_id = client['id'])
    visits = []
    for row in visit_q:
        status = ""
        if row.status == 0:
            status = "актуальный"
        if row.status == 1:
            status = "выполнен"
        if row.status == 2:
            status = "отменен"
        cabinet = get_cabinet(row.cabinet_id)['name']
        doctor = get_worker(row.doctor_id)
        pet = get_pet(token, row.pet_id)
        visit = dict(
            id = row.id,
            datetime = str(row.date),
            status = status,
            cabinet = cabinet,
            duration = row.duration,
            filial = get_filial(row.filial_id),
            pet = pet,
            doctor = doctor)
        visits.append(visit)
    return visits

@query.field("get_visit_by_pet")
def get_visit_by_pet(_, info, pet_id):
    logging('get_visit_by_pet')
    token = request.headers['token']
    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    Session = sessionmaker(bind=engine)
    session = Session()
    visit_q = session.query(Visit).filter_by(client_id = client['id'], pet_id = pet_id, date_of_delete = None)
    visits = []
    for row in visit_q:
        status = ""
        if row.status == 0:
            status = "актуальный"
        if row.status == 1:
            status = "выполнен"
        if row.status == 2:
            status = "отменен"
        cabinet = get_cabinet(row.cabinet_id)['name']
        doctor = get_worker(row.doctor_id)
        pet = get_pet(token, row.pet_id)
        visit = dict(
            id = row.id,
            datetime = str(row.date),
            status = status,
            cabinet = cabinet,
            duration = row.duration,
            filial = get_filial(row.filial_id),
            pet = pet,
            doctor = doctor)
        visits.append(visit)
    return visits

@mutation.field("add_visit")
def add_visit(_, info, input):
    logging('add_visit')
    token = request.headers['token']
    service_id = input['service_id']
    worker_id = input['worker_id']
    pet_id = input['pet_id']
    date = input['date']
    filial_id = input['filial_id']
    if service_id == '' or worker_id == '' or pet_id == '' or date == '' or filial_id == '':
        raise Exception('Не все данные переданы')

    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    service = get_service(service_id)
    cost = service["cost"]
    duration = service["duration"]

    try:
        workers = get_workers_by_service_(service_id)
    except:
        raise Exception('Врачи, оказывающие услугу не найдены')

    worker = worker_id

    k = 0
    for w in workers:
        if w["id"] == worker_id:
            k = 1
            break
    if k != 1:
        raise Exception('Ошибка, работник не оказывает данную услугу')

    pet = get_pet(token, pet_id)
   
    cabinet_id = get_cabinets_by_schedule(filial_id, date, worker_id) 
    if not cabinet_id:
        raise Exception('Ошибка,кабинет не найден')
    try:
        insert_stmt = insert(Visit).values(
            date = date,
            cost = cost,
            filial_id = filial_id,
            doctor_id = worker_id,
            pet_id = pet_id,
            client_id = client["id"],
            status = 0,
            duration = duration,
            cabinet_id = cabinet_id)
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        visit_id = result.inserted_primary_key
        return visit_id[0]
    except:
        raise Exception('Ошибка,прием не создан')

@mutation.field("cancel_visit")
def cancel_visit(_, info, visit_id):
    logging('cancel_visit')
    token = request.headers['token']
    try:
        client = get_client_by_user(token)
    except:
        raise Exception('Ошибка авторизации')
    try:    
        update_stmt = visit_table.update().values(status = 2).where(and_(visit_table.c.id == visit_id, visit_table.c.client_id == client["id"]))
        conn = engine.connect()
        result = conn.execute(update_stmt)
        return True
    except:
        return False


#CABINET#############################################################

def get_cabinet(id):
    Session = sessionmaker(bind=engine)
    session = Session()
    cabinet_q = session.query(Cabinet).filter_by(id = id).all()
    cabinet = ''
    for row in cabinet_q:
        cabinet = dict(
            id = row.id,
            name = row.name,
            description = row.description,
            filial_id = row.filial_id)
    return cabinet

def get_cabinets_by_schedule(filial_id, date, worker_id):
    date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    cabinet = 0
    Session = sessionmaker(bind=engine)
    session = Session()
    schedule_q = session.query(Schedule).filter_by(filial_id = filial_id, worker_id = worker_id).all()
    for row in schedule_q:
        if (row.time1_start != None) and (row.time1_start < date) and (row.time1_end > date):
            return row.cabinet_id
        elif (row.time2_start != None) and (row.time2_start < date) and (row.time2_end > date):
            return row.cabinet_id
        elif (row.time3_start != None) and (row.time3_start < date) and (row.time3_end > date):
            return row.cabinet_id
    return cabinet

#SCHEDULE############################################################

@query.field("get_free_date_schedule")
def get_free_date_schedule(_, info, filial_id, worker_id):
    logging('get_free_date_schedule')

    dates = []
    connect = pymysql.connect(
        host = '127.0.0.1',
        user = 'maria',
        db ='vc_2',
        password = '1234',
        charset ='utf8mb4')
    with connect:
        cur = connect.cursor()
        sql = """SELECT id, time1_start FROM schedule
        WHERE worker_id = %s and filial_id = %s and time1_start > CURDATE() """ % (worker_id, filial_id)
        cur.execute(sql)
        rows = cur.fetchall()
        dates = []
        for row in rows:
            date = row[1].date()
            d = dict(
                schedule_id = row[0],
                date = str(date))
            dates.append(d)
        return dates

@query.field("get_free_time_schedule")
def get_free_time_schedule(_, info, schedule_id, service_id):
    logging('get_free_time_schedule')
    
    service = get_service(service_id)
    duration = int(service["duration"])

    Session = sessionmaker(bind=engine)
    session = Session()
    schedules_q = session.query(Schedule).filter_by(id = schedule_id).all()

    for row in schedules_q:
        schedule = dict(
            id = row.id,
            worker_id = row.worker_id,
            time1_start = row.time1_start,
            time1_end = row.time1_end)
        date = row.time1_start.date()

    visits_q = session.query(Visit).filter(func.date(Visit.date) == date).all()
    print(visits_q)
    visits = {}
    for row in visits_q:
        date_end = row.date + datetime.timedelta(minutes = row.duration)
        visit = dict(
            doctor_id = row.doctor_id,
            date_start = row.date,
            date_end = date_end)
        key = row.id
        visits[key] = visit
    
    # dt = schedule["time1_start"] - datetime.timedelta(minutes = 20)  
    for visit in visits:
        for v in visits:
            if visits[visit]["date_start"] < visits[v]["date_start"]:
                vvv = visits[v]
                visits[v] = visits[visit]
                visits[visit] = vvv
    # duration
    times = {}
    time = {}
    dt = schedule["time1_start"]  
    i = 1
    
    for visit in visits:
        if visits[visit]["doctor_id"] == schedule["worker_id"]:
            while dt < schedule["time1_end"] :
                if dt >= visits[visit]["date_start"] and dt <= visits[visit]["date_end"]:
                    dt = visits[visit]["date_end"] + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
                    break
                else: 
                    time["date_end"] = str(dt) #end
                    time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
                    times[i] = time.copy()
                    i += 1
                    dt = dt + datetime.timedelta(minutes = 5)


    while dt <= schedule["time1_end"]:
        dt = dt + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
        if dt <= schedule["time1_end"]:
            time["date_end"] = str(dt) #end
            time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
            times[i] = time.copy()
            i += 1
    
    free_times = []
    n = 0
    for time in times:
        n = n + 1
        t = dict(id = n, time = times[time]["date_start"])
        free_times.append(t)
    
    return free_times


#ROUTE###############################################################

schema = make_executable_schema(type_defs, query, mutation)

@app.route("/graphql", methods=["GET"])
def graphql_playgroud():
    return PLAYGROUND_HTML, 200

@app.route("/graphql", methods=["POST"])
def graphql_server():
    # data = request.get_json()
    data = request.get_json()

    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )

    status_code = 200 if success else 400
    return jsonify(result), status_code




#####################################################################

def logging(ev_name): #запись в журнал
    client_addr = request.remote_addr
    event = f"{client_addr}: '{ev_name}'"
    row = f"{str(datetime.datetime.now())} - {event}\n"
    mutex = WinMutex("mutex_vet_clinic")
    while True:
        mutex.wait_mutex(mutex.mutex)
        event_log = open('event_log.txt', 'a')
        event_log.write(row)
        event_log.close()
        mutex.release()
        return

#CACHE###############################################################

def get_cache(request_name, id):
    data = ''
    answer = {}
    answer = dict(set = True)
    try:
        my_server = redis.Redis(connection_pool=POOL)
        if id == 0:
            data = my_server.get(request_name)
        else:
            data = my_server.get(request_name + " " + str(id))
    except:
        answer["set"] = False
        print("Ошибка подключения к Redis")
    if data != "" and data != None:
        print("Берем данные из кэша")
        if id == 0:
            answer[request_name] = json.loads(data)
        else:
            answer[request_name + ' ' + str(id)] = json.loads(data)

        answer["set"] = False

    return answer

def set_cache(request_name, answer, state_set):
    if state_set:
        try:
            my_server = redis.Redis(connection_pool=POOL)
            with my_server.pipeline() as pipe:
                pipe.set(request_name, bytes(json.dumps(answer), 'UTF-8'))
                pipe.execute()
                my_server.bgsave()
                print("Данные добавлены в кэш")
        except:
            print("(!)Ошибка подключения к Redis")
    else:
        print("(!)Данне не закэшированы")

#MUTEX###############################################################
class WinMutex:
    def __init__(self, name):
        self.mutexname = name
        self.mutex = win32event.CreateMutex(None, 1, self.mutexname)
        self.lasterror = win32api.GetLastError()

    def release(self):
        return win32event.ReleaseMutex(self.mutex)

    def wait_mutex(self, mutex):
        win32event.WaitForSingleObject(mutex, win32event.INFINITE)  

if __name__ == "__main__":
    app.run(debug=True)